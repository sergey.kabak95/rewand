var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync  = require('browser-sync'),
    rigger = require('gulp-rigger'),
    autoprefixer = require('gulp-autoprefixer'),
    spritesmith = require('gulp.spritesmith'),
    cssnano = require('gulp-cssnano');

gulp.task('sass', function() {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(['last 5 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'src/',
            index: 'html/products.html'
        },
        notify: false,
        open: false,
        // tunnel: true,
        // tunnel: "tunnelrewand" //Demonstration page: http://projectname.localtunnel.me
    });
});

gulp.task('html', function () {
    gulp.src('src/templates/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('src/html/'));
});

gulp.task('sprite', function () {
    var spriteData = gulp.src('src/img/index/sprite/*.png').pipe(spritesmith({
        imgName: 'sprite-dist.png',
        cssName: 'sprite-dist.css'
    }));
    
    return spriteData.pipe(gulp.dest('src/img/index/sprite_dist/'));
});

gulp.task('cssmini', function() {
    return gulp.src('src/css/index.css')
        .pipe(cssnano())
        .pipe(gulp.dest('dist/css/'));
});

gulp.task('watch', ['browser-sync', 'sass', 'html'], function () {
    gulp.watch('src/scss/**/*.scss', ['sass']);
    gulp.watch('src/templates/**/*.html', ['html']);
    gulp.watch('src/html/**/*.html', browserSync.reload);
    gulp.watch('src/css/**/*.css', browserSync.reload);
    gulp.watch('src/js/**/*.js', browserSync.reload);
});

gulp.task('default', ['watch']);