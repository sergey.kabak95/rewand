$(document).ready(function (){
	if( $(window).width() < 768 ){
		$('.form_search').insertAfter('.menu-burger');
		$('.dropdown').insertAfter('.header__logo');
		$('.mobile-container').append( $('.manual_bar') );
		$(window).scroll(function () {
			if ( $(this).scrollTop() > 0 ){
				$('.manual_bar').fadeOut();
			}
			else{
				$('.manual_bar').fadeIn();
			}
		});
	}

	$(window).resize(function () {
		if( $(window).width() < 768 ){
			$('.form_search').insertAfter('.menu-burger');
			$('.dropdown').insertAfter('.header__logo');
			$('.manual_bar').insertAfter('.mobile-container');
			$('.navbar').removeClass('flex-menu');
		}
		else{
			$('.user-container').append( $('.manual_bar') );
			$('.header__form').append( $('.form_search') );
			$('.dropdown').insertBefore('.header_account');
			$('.navbar').addClass('flex-menu');
		}
	});

	// header search field style on focus
	$('.search-field').focus(function () {
		$(this).parent().css({
			'border' : '1px solid #cecece'
		});
	});
	$('.search-field').focusout(function () {
		$(this).parent().css({
			'box-shadow' : '0 0 15px transparent',
			'border' : '1px solid transparent'
		});
	});

	$('.recommended-goods__wrapper').slick({
		slidesToShow: 5,
		infinite: true,
		centerMode: false,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 4,
					infinite: true,
					centerMode: false,
					slidesToScroll: 1,
					dots: true,
					arrows: true
				}
			}
		]
	});

	// slick slider config
	$('.page-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		speed: 500,
		fade: true,
		cssEase: 'linear',
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					speed: 500,
					fade: true,
					cssEase: 'linear',
					dots: true,
					arrows: false
				}
			}
		]
	});

	// top menu burger (mobile)
	var burger = $('.mobile-menu').find('.menu-burger');

	burger.click(function () {
		$('.navbar').slideToggle();
		$('.navbar').toggleClass('navbar-animation');
	});


	$(window).scroll(function () {
		if( $(this).scrollTop() > 70 ){
			$('.menu-wrapper').addClass('fixed-menu');
		}else {
			$('.menu-wrapper').removeClass('fixed-menu');
		}
	});
});

// header language dropdown
$('.dropdown-toggle').click(function (){
	$('.dropdown-menu').toggle();
});


// button TO TOP
$('.to-top__container').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 1000);
    return false;
});

// products small photo click
$('.product-photo').click(function () {
	$('.product-photo').removeClass('photo-active');
	$(this).addClass('photo-active');
});

// product select color
$('.choice-color').click(function () {
	$('.choice-color').parent().removeClass('color-selected');
	$(this).parent().addClass('color-selected');
});